import { Container, Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, Pagination} from "@mui/material";
import { useEffect, useState } from "react";

function DataTable() {
    const numRow = 3;
    const [rows, setRows] = useState([]);
    const [totalPage, setTotalPage] = useState(0);
    const [ curPage, setCurPage] = useState(1)

    const fetchAPI = async (url) => {
        const response = await fetch(url);
        const data = response.json();

        return data;
    }

    const handleChangePage = (event, value) => {
        setCurPage(value)
    }

    useEffect(() => {
        fetchAPI("https://jsonplaceholder.typicode.com/users").then((data) => {
            
            setTotalPage(Math.ceil(data.length/numRow));
            setRows(data.slice((curPage-1) * numRow, curPage*numRow));
        })
        .catch((error) => {
            console.error(error.message)
        })
    }, [curPage])
    return (
        <Container>
            <Grid item xs={12} sm={12} md={12} lg={12}>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">ID</TableCell>
                                <TableCell align="left">Name</TableCell>
                                <TableCell align="left">Username</TableCell>
                                <TableCell align="left">Email</TableCell>
                                <TableCell align="left">Phone</TableCell>
                                <TableCell align="left">Website</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row) => (
                                <TableRow
                                    key={row.id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell align="left">{row.name}</TableCell>
                                    <TableCell align="left">{row.username}</TableCell>
                                    <TableCell align="left">{row.email}</TableCell>
                                    <TableCell align="left">{row.phone}</TableCell>
                                    <TableCell align="left">{row.website}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
            <Grid container justifyContent={"end"}>
                <Grid item>
                    <Pagination count={totalPage} defaultPage={curPage} onChange={handleChangePage}></Pagination>
                </Grid>
            </Grid>
        </Container>
    )
}

export default DataTable;